
const video = document.getElementById('video')
var synth = window.speechSynthesis;


Promise.all([
  faceapi.nets.ssdMobilenetv1.loadFromUri('/models'),
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'), // withFaceLandmarks
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'), // withFaceDescriptors
  // faceapi.nets.faceExpressionNet.loadFromUri('/models')
  ]).then(startVideo)

async function startVideo() {
  navigator.getUserMedia(
    { video: {
        width: { ideal: 4096 },
        height: { ideal: 2160 }
    }  },
    stream => video.srcObject = stream,
    err => console.error(err)
    )
}

let lastPersonVisibleState = -1;
let personVisibleState = 0; // 0 = no-one visible, 1 = person visible
let tryMatchPerson = true;
let results, ref;

video.addEventListener('play', async() => {

  if (tryMatchPerson) {
    ref = document.querySelector('.ref');
    // const refCanvas = faceapi.createCanvasFromMedia(ref);

    results = await faceapi
    .detectAllFaces(ref)
    .withFaceLandmarks()
    .withFaceDescriptors()

    // console.log(results);
  }

  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  document.getElementById('videocontainer').appendChild(canvas);
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)

  setInterval(async() => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())
                                                               // .withFaceLandmarks()
                                                               // .withFaceExpressions()

                                                               const resizedDetections = faceapi.resizeResults(detections, displaySize)

    // remove label
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)

    const personIsFound = resizedDetections.length > 0 && resizedDetections[0].box;

    // if person found in this iteration, update and show the name box
    if (personIsFound) {
      // try to match the person
      let personName = 'Onbekend';

      if (tryMatchPerson) {
        const faceMatcher = new faceapi.FaceMatcher(results)

        const singleResult = await faceapi
        .detectSingleFace(video)
        .withFaceLandmarks()
        .withFaceDescriptor()

        if (singleResult) {
          const bestMatch = faceMatcher.findBestMatch(singleResult.descriptor)
console.log(bestMatch);
          personName = bestMatch.label;//ref.getAttribute('data-name');
          // console.log(`hoi ${personName}`, bestMatch);

          if (personName != 'unknown') {
            personName = ref.getAttribute('data-name');
            text2Speech(`Hoi ${personName}`);
          }

          // text2Speech(`Hoi ${personName}`);
        }
      } else {
        personName = '';
      }

      const box = resizedDetections[0].box;
      const drawBox = new faceapi.draw.DrawBox(box, { label: personName,   lineWidth: 5, boxColor : 'red', drawLabelOptions: { backgroundColor: 'black', fontColor: 'white', anchorPosition: 'TOP_LEFT' } });
      drawBox.draw(canvas);

      personVisibleState = 1;
    } else {
      personVisibleState = 0;
    }

    if (lastPersonVisibleState == -1) {
      // console.log(`het begin: persoon is ${personVisibleState == 0 ? 'NIET' : 'WEL'} herkend`)
    }
    else {
      if (personVisibleState != lastPersonVisibleState) {
        if (personVisibleState == 0) {
          // console.log('persoon is niet meer zichtbaar');
        } else {
          // console.log('persoon is wel weer zichtbaar');
        }
      }

      // console.log(`tussenstand: persoon ${personVisibleState == 0 ? 'NIET' : 'WEL'} herkend`)
    }

    lastPersonVisibleState = personVisibleState;

    // faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    // faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
  }, 1000)
})

/*
(async () => {
  await faceapi.nets.ssdMobilenetv1.loadFromUri('/models');
  // await faceapi.nets.tinyFaceDetector.loadFromUri('/models');
  await faceapi.nets.faceLandmark68Net.loadFromUri('/models');
  await faceapi.nets.faceExpressionNet.loadFromUri('/models');
  await faceapi.nets.faceRecognitionNet.loadFromUri('/models');

  // console.log('going');

  doIt(document.querySelector('.image1'), true);
  // doIt(document.querySelector('.image2'));

})();

async function doIt(image, usetiny = false) {
  // const image = document.querySelector('img');
  const canvas = faceapi.createCanvasFromMedia(image);
  // const detection = await faceapi.detectAllFaces(image, usetiny ? new faceapi.TinyFaceDetectorOptions() : new faceapi.SsdMobilenetv1Options())
  const detection = await faceapi.detectAllFaces(image)
                          // .withFaceLandmarks()
                          // .withFaceExpressions();

  const resizedDimensions = faceapi.resizeResults(detection, { width: image.width, height: image.height });

  document.body.append(canvas);
  faceapi.draw.drawDetections(canvas, resizedDimensions);
  // faceapi.draw.drawFaceLandmarks(canvas, resizedDimensions);
  // faceapi.draw.drawFaceExpressions(canvas, resizedDimensions);

  const ref = document.querySelector('.ref');
  const refCanvas = faceapi.createCanvasFromMedia(ref);

  const results = await faceapi
    .detectAllFaces(ref)
    .withFaceLandmarks()
    .withFaceDescriptors()

  const resizedDetections = faceapi.resizeResults(results, { width: ref.width, height: ref.height });
  // $('#ref').append(refCanvas);
  document.body.append(refCanvas);
  document.getElementById('ref').appendChild(refCanvas);
  faceapi.draw.drawDetections(refCanvas, resizedDetections)
  // faceapi.draw.drawFaceLandmarks(refCanvas, resizedDetections);
  // faceapi.draw.drawFaceExpressions(refCanvas, resizedDetections);


  if (!results.length) {
    return
  }

  // create FaceMatcher with automatically assigned labels
  // from the detection results for the reference image
  const faceMatcher = new faceapi.FaceMatcher(results)
  // console.log(faceMatcher);

  const singleResult = await faceapi
    .detectSingleFace(image)
    .withFaceLandmarks()
    .withFaceDescriptor()

    if (singleResult) {
      const bestMatch = faceMatcher.findBestMatch(singleResult.descriptor)
      console.log(bestMatch.toString())
    }

console.log('-----------');

const multipleresults = await faceapi
  .detectAllFaces(image)
  .withFaceLandmarks()
  .withFaceDescriptors()

multipleresults.forEach(fd => {
  const bestMatch = faceMatcher.findBestMatch(fd.descriptor)
  console.log(bestMatch)
  console.log(bestMatch.toString())
  if (!bestMatch.toString().includes('unknown')) {
    console.log('gematched!!');
  }
console.log('-----------');
})

}
*/

let voices, ourVoice;

function populateVoiceList() {
  voices = synth.getVoices();
  for (const voice of voices) {
    if (voice.lang.toUpperCase() == 'NL-NL') {
      ourVoice = voice;
      break;
    }
  }

  // .sort(function (a, b) {
  //     const aname = a.name.toUpperCase(), bname = b.name.toUpperCase();
  //     if ( aname < bname ) return -1;
  //     else if ( aname == bname ) return 0;
  //     else return +1;
  // });

  // var selectedIndex = voiceSelect.selectedIndex < 0 ? 0 : voiceSelect.selectedIndex;
  // voiceSelect.innerHTML = '';
  // for(i = 0; i < voices.length ; i++) {
  //   var option = document.createElement('option');
  //   option.textContent = voices[i].name + ' (' + voices[i].lang + ')';

  //   if(voices[i].default) {
  //     option.textContent += ' -- DEFAULT';
  //   }

  //   option.setAttribute('data-lang', voices[i].lang);
  //   option.setAttribute('data-name', voices[i].name);
  //   voiceSelect.appendChild(option);
  // }
  // voiceSelect.selectedIndex = selectedIndex;
}

populateVoiceList();
if (speechSynthesis.onvoiceschanged !== undefined) {
  speechSynthesis.onvoiceschanged = populateVoiceList;
}

let allowSpeech = true;

function text2Speech(text) {
  if (synth.speaking) {
      // console.error('speechSynthesis.speaking');
      return;
  }

  if (!allowSpeech) {
      // console.error('speaking not allowed yet');
      return;
  }

  var utterThis = new SpeechSynthesisUtterance(text);

  utterThis.onend = function (event) {
    // console.log('SpeechSynthesisUtterance.onend; disable speech for 10 secs');
    allowSpeech = false;
    window.setTimeout(() => {
      allowSpeech = true;
      // console.log('speech allowed again');
    }, 10000);
  }
  utterThis.onerror = function (event) {
    console.error('SpeechSynthesisUtterance.onerror');
  }

  // var selectedOption = 'nl-NL';//voiceSelect.selectedOptions[0].getAttribute('data-name');
  // for(i = 0; i < voices.length ; i++) {
  //   if(voices[i].name === selectedOption) {
  //     utterThis.voice = voices[i];
  //     break;
  //   }
  // }

  utterThis.voice = ourVoice ? ourVoice : voices[0];

  // utterThis.pitch = pitch.value;
  // utterThis.rate = rate.value;
  synth.speak(utterThis);
}
